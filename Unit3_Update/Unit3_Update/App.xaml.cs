﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Unit3_Update
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();//初始化
            ContentPage page = new ContentPage();//建立Page
            Button btn = new Button() { Text = "Hello Xamarin Forms" };//建立按鈕
            btn.Clicked += (sender, e) =>//註冊按鈕事件
            {
                page.DisplayAlert("Xamarin", "Hello Xamarin", "OK");//Alert訊息
            };
            page.Content = btn;//將控制項放到Page中
            MainPage = page;//將Page放到Application的MainPage屬性
  
        }

    

        
    }
}
